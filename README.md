# Vacuum Pump Controller Leybold TD20

EPICS module to provide communications and read/write data from/to **Leybold TD20 vacuum pump controllers** and pumps

**Please note that the pump has to be configured together with the controller i.e. in the same IOC**

There are separate `.iocsh` scripts for the controller and the pump.

## IOCSH files

*   Controller: [vac_ctrl_leyboldtd20_moxa.iocsh](iocsh/README.md#vac_ctrl_leyboldtd20_moxa)
*   Pump: [vac_pump_leyboldtd20_vpt.iocsh](iocsh/README.md#vac_pump_leyboldtd20_vpt)

## MOXA configuration

#### Operation Modes

```
Application: Socket
Mode: TCP Server
.
.
.
Packing length: 0
Delimiter 1: 00 / Disabled
Delimiter 2: 00 / Disabled
Delimiter process: Do Nothing
Force transmit: 0
```

#### Communication Parameters

```
Baud rate: 19200
Data bits: 8
Stop bits: 1
Parity: Even
Flow control: None
Interface: RS-232
```
